from PostgreSQL.postgres_con import DubleExecute
import Functions.global_ as global_

def create_dummy_table():
	DubleExecute("CREATE TABLE IF NOT EXISTS dummy_table(id_dummy_table SERIAL NOT NULL,given_name VARCHAR(45) NOT NULL,family_name VARCHAR(45) NOT NULL,PRIMARY KEY (id_dummy_table));", "INSERT INTO dummy_table(given_name,family_name) VALUES ('Marek','Smutný'),('David','Veselý'),('Markéta','Křivonohá')")

