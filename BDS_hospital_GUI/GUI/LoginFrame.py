import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import Functions.global_ as global_
from Functions.LoginFunc import Login
from Functions.Logging import Logger


class LoginFrame(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        controller.title("Hospital GUI")
        controller.geometry("900x500")
        controller.configure(bg = "#222831")
        self.canvas = tk.Canvas(self, bg = "#222831", height = 500, width = 900, bd = 0, highlightthickness = 0, relief = "ridge")
        self.canvas.place(x = 0, y = 0)
        self.canvas.create_rectangle(438.0, 0.0, 900.0, 500.0, fill="#FCFCFC", outline="")

        self.canvas.create_text(493.0, 217.0, anchor="nw", text="Password", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.canvas.create_text(493.0, 82.0, anchor="nw", text="Username", fill="#222831", font=("Roboto Bold", 24 * -1) )
        self.hlaska = self.canvas.create_text(490.0, 20.0, anchor="nw", text="", fill="#FF0000", font=("Roboto Bold", 24 * -1) )


        self.entry_image_1 = tk.PhotoImage(file="./assets/entry_1.png")
        entry_bg_1 = self.canvas.create_image(665.5, 164.5, image=self.entry_image_1 ) 
        self.entry_1 = tk.Entry(self, bd=0, bg="#E5E5E5", highlightthickness=0, font=("Roboto Bold", 20 * -1)) 
        self.entry_1.place(x=505.0, y=134.0, width=321.0, height=59.0 )

        self.button_image_3 = tk.PhotoImage(file="./assets/button_3.png")
        button_3 = tk.Button(self, image=self.button_image_3, borderwidth=0, highlightthickness=0, command=lambda: self.runLogin(), relief="flat") 
        button_3.place(x=576.0, y=379.0, width=180.0, height=55.0 )

        self.entry_image_2 = tk.PhotoImage(file="./assets/entry_2.png")
        entry_bg_2 = self.canvas.create_image(665.5, 297.5, image=self.entry_image_2 ) 

        self.entry_2 = tk.Entry(self, bd=0, bg="#E4E4E5",show="*", highlightthickness=0, font=("Roboto Bold", 20 * -1)) 
        self.entry_2.place(x=505.0, y=267.0, width=321.0, height=59.0 )


        self.button_image_4 = tk.PhotoImage(file="./assets/button_4.png")
        button_4 = tk.Button(self, image=self.button_image_4, borderwidth=0, highlightthickness=0, command=lambda: controller.show_frame("StartFrame"), relief="flat") 
        button_4.place(x=22.0, y=18.0, width=42.0, height=34.0 )

        self.image_image_1 = tk.PhotoImage(file="./assets/image_1.png")
        image_1 = self.canvas.create_image(206.0, 267.0, image=self.image_image_1 ) 
        controller.resizable(False, False)

        self.entry_2.bind('<Return>',self.runLogin)

    def runLogin(self, event=None):
        username = self.entry_1.get()
        password = self.entry_2.get()
        proces = Login(username,password)
        if(proces == False):
            Logger("Incorrect login attempt with username: " +  username)
            self.canvas.itemconfig(self.hlaska, text = "Username or password is not valid")
            
        else:
            self.controller.showData()
            self.controller.show_frame("MainFrame")
