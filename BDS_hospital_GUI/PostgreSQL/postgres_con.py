import psycopg2, json
from PostgreSQL.con_config import config
from Functions.Logging import Logger


def Execute(command, case):
    conn = None
    try:
        print(command)
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(command)
        if case == 1:
            result = cur.fetchone()[0]
            cur.close()
            return result
        elif case == 2:
            result = cur.fetchall()
            cur.close()
            return result
        elif case == 3:
            result = [desc[0] for desc in cur.description]
            cur.close()
            return result
        elif case == 4:
            conn.commit()
            cur.close()
        
    except (Exception, psycopg2.DatabaseError) as error:
        Logger(error)
        print(error)
    finally:
        if conn is not None:
            conn.close()

def DubleExecute(command1,command2):
    conn = None
    try:
        print("Provádím query: "+command1+" společně s query : " + command2)
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(command1)
        cur.execute(command2)
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        Logger(error)
        print(error)

    finally:
        if conn is not None:
            conn.close()


