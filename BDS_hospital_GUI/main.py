import tkinter as tk
from tkinter import font as tkfont
from tkinter import ttk
import GUI.LoginFrame as LoginFrame
import GUI.MainFrame as MainFrame
import GUI.AddRecordFrame as AddRecordFrame
import GUI.EditRecordFrame as EditRecordFrame
import GUI.DetailedViewFrame as DetailedViewFrame
import GUI.InjectionFrame as InjectionFrame
import Functions.global_ as global_


class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (LoginFrame.LoginFrame, MainFrame.MainFrame, AddRecordFrame.AddRecordFrame, EditRecordFrame.EditRecordFrame, DetailedViewFrame.DetailedViewFrame, InjectionFrame.InjectionFrame):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        self.show_frame("LoginFrame")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()

    def detailedView(self):
        frame = self.frames["DetailedViewFrame"]
        frame.showData()
    
    def printBoxes(self):
        frame = self.frames["AddRecordFrame"]
        frame.printBoxes()

    def showData(self):
        frame = self.frames["MainFrame"]
        frame.fillComboBox()
        frame.showData()

    def addNewRecord(self,record_username,record_password,website):
        frame = self.frames["MainFrame"]
        frame.addNewRecord(record_username,record_password,website)

    def printBoxes_Edit(self, values):
        frame = self.frames["EditRecordFrame"]
        frame.printBoxes(values)

    def printBoxes_Injection(self, values):
        frame = self.frames["InjectionFrame"]
        frame.printBoxes(values)

if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()