-- > **Install Requirements**

      pip install -r requirements.txt

-- > **Export database into PostgreSQL**
    
use [POSTGRES_SQL.sql](https://gitlab.com/xsladk12/bds-hospital-gui/-/blob/main/POSTGRES_SQL.sql) file

-- > **Change Login credentials for database connection**

change inside [database.ini](https://gitlab.com/xsladk12/bds-hospital-gui/-/blob/main/BDS_hospital_GUI/database.ini) configuration file


-- > **Run program**

      python main.py


- > **Cíl aplikace:**

 Cílem aplikace bylo docílit přehledného prostředí pro zaměstnance nemocnice. V aplikaci je možné nalézt jednotlivé záznamy pacientů, procedůr a  pobytů. Jednotlivé záznamy je možné upravovat, mazat nebo přidávat.

       

 




